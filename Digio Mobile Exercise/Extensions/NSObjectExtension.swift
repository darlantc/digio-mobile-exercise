//
//  NSObjectExtension.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import Foundation

extension NSObject {
	@objc var className: String {
		return String(describing: type(of: self))
	}
	
	@objc class var className: String {
		return String(describing: self)
	}
}
