//
//  UrlSessionHttpService.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import Foundation

class UrlSessionHttpService: HttpServiceProtocol {
	func get(urlString: String, completionHandler: @escaping (ApiResponseModel) -> Void) {
		guard let url = URL(string: urlString) else {
			return
		}
		
		URLSession.shared.dataTask(with: url) { (data, response, error) in
			guard let httpResponse = response as? HTTPURLResponse else {
				let response = ApiResponseModel(
					statusCode: 500,
					data: nil,
					error: true
				)
				completionHandler(response)
				return
			}
			let response: ApiResponseModel
			if let data = data {
				response = ApiResponseModel(
					statusCode: httpResponse.statusCode,
					data: data,
					error: false
				)
			}
			else {
				response = ApiResponseModel(
					statusCode: httpResponse.statusCode,
					data: nil,
					error: true
				)
			}
			
			completionHandler(response)
		}.resume()
	}
}
