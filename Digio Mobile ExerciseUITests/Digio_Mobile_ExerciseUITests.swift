//
//  Digio_Mobile_ExerciseUITests.swift
//  Digio Mobile ExerciseUITests
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import XCTest

class Digio_Mobile_ExerciseUITests: XCTestCase {
	var app: XCUIApplication!

    override func setUpWithError() throws {
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        app = XCUIApplication()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        app.launch()
		
		XCTAssertTrue(app.otherElements["mainViewController"].exists)
		
		// Wait for greetingsLabel change text when network call succeed
		let predicate = NSPredicate(format: "label BEGINSWITH 'Olá,'")
		expectation(for: predicate, evaluatedWith: app.staticTexts.element(matching: predicate), handler: nil)
		waitForExpectations(timeout: 5, handler: nil)
		
		// Verify Labels
		XCTAssertTrue(app.staticTexts.element(matching: .any, identifier: "digio").exists)
		XCTAssertTrue(app.staticTexts.element(matching: .any, identifier: "Cash").exists)
		XCTAssertTrue(app.staticTexts.element(matching: .any, identifier: "Produtos").exists)
		
		// Verify other views
		XCTAssertTrue(app.collectionViews["spotlightCollectionView"].exists)
		XCTAssertTrue(app.collectionViews["productsCollectionView"].exists)
		XCTAssertTrue(app.images["digioCashImageView"].exists)
		
		// Verify and tap transparent cash button
		let digioCashButton = app.buttons["digioCashButton"]
		XCTAssertTrue(digioCashButton.exists)
		digioCashButton.tap()
		
		// Verify alert opens with OK button
		XCTAssertTrue(app.alerts.element.buttons["OK"].exists)
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
