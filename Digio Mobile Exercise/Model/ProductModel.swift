//
//  ProductModel.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import Foundation

struct ProductModel: Codable {
	var name: String
	var imageUrl: String
	var description: String
	
	enum CodingKeys: String, CodingKey {
		case name
		case imageUrl = "imageURL"
		case description
	}
}
