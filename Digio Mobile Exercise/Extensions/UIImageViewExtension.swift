//
//  UIImageExtension.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
	func rounded() {
		self.layer.cornerRadius = kCornerRadius
	}
	
	func setNetworkImage(imageUrl: String) {
		guard let url = URL(string: imageUrl) else {
			return
		}
		
		self.kf.indicatorType = .activity
		self.kf.setImage(
			with:url,
			options: [
				.transition(.fade(1)),
			]
		)
	}
}
