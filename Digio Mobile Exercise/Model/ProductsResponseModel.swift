//
//  ProductsResponseModel.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import Foundation

struct ProductsResponseModel: Codable {
	var cash: CashModel
	var spotlight: [SpotlightModel]
	var products: [ProductModel]
}
