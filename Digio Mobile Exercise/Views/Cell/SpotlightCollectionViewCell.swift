//
//  SpotlightCollectionViewCell.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import UIKit

class SpotlightCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak fileprivate var innerView: UIView!
    @IBOutlet weak fileprivate var imageView: UIImageView!
	
	func setImage(_ imageUrl: String) {
		self.innerView.roundedWithShadow()
		self.imageView.rounded()
		self.imageView.clipsToBounds = true
		self.imageView.setNetworkImage(imageUrl: imageUrl)
	}
}
