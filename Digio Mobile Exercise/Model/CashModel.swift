//
//  CashModel.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import Foundation

struct CashModel: Codable {
	var title: String
	var bannerUrl: String
	var description: String
	
	enum CodingKeys: String, CodingKey {
		case title
		case bannerUrl = "bannerURL"
		case description
	}
}
