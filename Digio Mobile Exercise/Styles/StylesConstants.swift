//
//  StylesConstants.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import Foundation
import UIKit

let kCornerRadius: CGFloat = 8.0
