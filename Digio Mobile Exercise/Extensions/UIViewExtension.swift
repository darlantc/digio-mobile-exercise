//
//  UIViewExtension.swift
//  Digio Mobile Exercise
//
//  Created by Darlan ten Caten on 18/09/20.
//  Copyright © 2020 Darlan Tódero ten Caten. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
	func roundedWithShadow() {
		self.layer.cornerRadius = kCornerRadius
		layer.masksToBounds = false
		layer.shadowColor = UIColor.black.cgColor
		layer.shadowOpacity = 0.2
		layer.shadowOffset = CGSize(width: 0, height: 3)
	}
}
